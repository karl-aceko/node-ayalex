import { reservation } from "../data/reservation.js";
import { handleReservations } from './getReservationsRequests.js';

// Gère les requêtes GET
const handleGetRequest = (request, response) => {
  const { url } = request;

  if(url === "/")
  {
    response.end("GET /, /reservation");
    return;
  }

  if(url.startsWith("/reservation")) {
    handleReservations(request, response);
    return;
  }

  response.writeHead(404);
  response.end("Not found"); 

}

export { handleGetRequest };