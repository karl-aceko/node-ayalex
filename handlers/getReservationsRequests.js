import { reservation } from "../data/reservation.js";

const handleReservations = (request, response) => {
  const pathnameParams = request.url.split("/");

  const getAllReservations = pathnameParams.length === 2;
  
  if(getAllReservations) {
    response.end(JSON.stringify(reservation));
    return;
  }

  
}

export { handleReservations};